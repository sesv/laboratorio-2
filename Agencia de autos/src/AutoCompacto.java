
public class AutoCompacto extends Veh�culo{
	public int CantidadDePasajeros;

	public AutoCompacto(int numeroDeSerieDelMotor, String marca, int a�o, int precio, int cantidadDePasajeros) {
		super(numeroDeSerieDelMotor, marca, a�o, precio);
		this.CantidadDePasajeros = cantidadDePasajeros;
	}

	@Override
	public String toString() {
		return "AutoCompacto [CantidadDePasajeros=" + this.CantidadDePasajeros + ", NumeroDeSerieDelMotor="
				+ this.NumeroDeSerieDelMotor + ", Marca=" + this.Marca + ", A�o=" + this.A�o + ", Precio=" + this.Precio + "]";
	}
	
}
