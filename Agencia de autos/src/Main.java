
public class Main {

	public static void main(String[] args) {
		AutoCompacto a = new AutoCompacto(123,"Chevrolet",2011,26700000,4);
		AutoDeLujo b = new AutoDeLujo(456,"Audi",2016,135000000,2);
		Vagoneta c = new Vagoneta(789,"Renault",2014,65800000,5);
		Camioneta d = new Camioneta(159,"Toyota",2012,45300000,150,2,2);
		System.out.println(a.toString());
		System.out.println(b.toString());
		System.out.println(c.toString());
		System.out.println(d.toString());
	}

}
