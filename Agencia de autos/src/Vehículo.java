
public class Veh�culo {
	public int NumeroDeSerieDelMotor;
	public String Marca;
	public int A�o;
	public int Precio;
	
	public Veh�culo(int numeroDeSerieDelMotor, String marca, int a�o, int precio) {
		this.NumeroDeSerieDelMotor = numeroDeSerieDelMotor;
		this.Marca = marca;
		this.A�o = a�o;
		this.Precio = precio;
	}

	@Override
	public String toString() {
		return "Veh�culo [NumeroDeSerieDelMotor=" + this.NumeroDeSerieDelMotor + ", Marca=" + this.Marca + ", A�o=" + this.A�o
				+ ", Precio=" + this.Precio + "]";
	}

}
