
public class AutoDeLujo extends Veh�culo{
	public int CantidadDePasajeros;

	public AutoDeLujo(int numeroDeSerieDelMotor, String marca, int a�o, int precio, int cantidadDePasajeros) {
		super(numeroDeSerieDelMotor, marca, a�o, precio);
		this.CantidadDePasajeros = cantidadDePasajeros;
	}

	@Override
	public String toString() {
		return "AutoDeLujo [CantidadDePasajeros=" + this.CantidadDePasajeros + ", NumeroDeSerieDelMotor="
				+ this.NumeroDeSerieDelMotor + ", Marca=" + this.Marca + ", A�o=" + this.A�o + ", Precio=" + this.Precio + "]";
	}
	
}
