
public class Camioneta extends Veh�culo{
	public int CapacidadDeCarga;
	public int CantidadDeEjes;
	public int CantidadDeRodadas;
	public Camioneta(int numeroDeSerieDelMotor, String marca, int a�o, int precio, int capacidadDeCarga,
			int cantidadDeEjes, int cantidadDeRodadas) {
		super(numeroDeSerieDelMotor, marca, a�o, precio);
		this.CapacidadDeCarga = capacidadDeCarga;
		this.CantidadDeEjes = cantidadDeEjes;
		this.CantidadDeRodadas = cantidadDeRodadas;
	}
	@Override
	public String toString() {
		return "Camioneta [CapacidadDeCarga=" + this.CapacidadDeCarga + ", CantidadDeEjes=" + this.CantidadDeEjes
				+ ", CantidadDeRodadas=" + this.CantidadDeRodadas + ", NumeroDeSerieDelMotor=" + this.NumeroDeSerieDelMotor
				+ ", Marca=" + this.Marca + ", A�o=" + this.A�o + ", Precio=" + this.Precio + "]";
	}
	
}
