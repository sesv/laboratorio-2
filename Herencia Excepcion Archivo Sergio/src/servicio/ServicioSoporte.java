
package servicio;

import Exception.LibroException;
import dao.Dao;
import data.Soporte;
import data.Autor;
import data.Miembro;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

/**
 *
 * @author fabian.giraldo
 */

public class ServicioSoporte {
    private Dao dao;
    private ArrayList<Soporte> soportes = null; //Debe ser mejorado notablemente.
    
    public ServicioSoporte(){
      this.dao = new Dao();
    }
    
    public void cargarSoportes(String archivo) throws FileNotFoundException, LibroException{
       this.soportes = this.dao.cargarSoportes(archivo);
    }
    
    public ArrayList<Soporte> getSoportes(){
      return this.soportes;
    }
    
    public Soporte buscarSoporteTitulo(String titulo){
        for (Soporte soporte: this.soportes){
            if (soporte.getTitulo().equals(titulo)){
                return soporte;
            }
        }
        return null;
    }
    
    public Soporte buscarSoporteAutor(Autor autor){
    	for (Soporte soporte: this.soportes){
    		if (soporte.getAutor().equals(autor)){
    			return soporte;
    		}
    	}
    	return null;
    }
    
    public Miembro cargarMiembro(String archivo)throws FileNotFoundException{
    	Scanner lectura = new Scanner(new File(archivo));
    	lectura.useDelimiter(",");
    	while(lectura.hasNext()){
    		if (lectura.equals("Miembro")){
    			Miembro miembro = null;
    			String nombre = lectura.next();
    			String apellido = lectura.next();
    			String direccion = lectura.next();
    			miembro = new Miembro(nombre,apellido,direccion);
    			return miembro;
    		}
		}
    	return null;
    }
}
