package data;

public class Miembro {
	public String nombre;
	public String apellido;
	public String direccion;
	
	public Miembro(String nombre, String apellido, String direccion) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.direccion = direccion;
	}

	public String getNombre() {
		return nombre;
	}

}
