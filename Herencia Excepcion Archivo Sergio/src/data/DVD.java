
package data;

import java.util.Arrays;

/**
*
* @author El papi riki
*/

public class DVD extends Soporte{
   private Autor[] actores;
   private int duracion;

   public DVD(String titulo, int duracion, Autor[] actores) {
       super(titulo);
       this.duracion = duracion;
       this.actores = actores;
   }

   public Autor[] getActores() {
       return actores;
   }

   public void setActores(Autor[] actores) {
       this.actores = actores;
   }

   public int getDuracion() {
       return duracion;
   }

   public void setDuracion(int duracion) {
       this.duracion = duracion;
   }

   @Override
   public String toString() {
       return "DVD{" + super.toString() +"actores=" + Arrays.toString(actores) + ", duracion=" + duracion + '}';
   }
   
}

