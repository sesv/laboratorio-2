
package data;

public class CD extends Soporte{
	public String genero;
	public int duracion;
	
	public CD(String titulo, String genero, int duracion) {
		super(titulo);
		this.genero = genero;
		this.duracion = duracion;
	}
	
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public int getDuracion() {
		return duracion;
	}
	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	@Override
	public String toString() {
		return "CD {" + super.toString() + ", genero=" + genero + ", duracion=" + duracion + "}";
	}

	
}
