
package Exception;

/**
*
* @author fabian.giraldo
*/

public class LibroException extends Exception{
   public LibroException(String mensaje){
      super(mensaje);
   }
}
