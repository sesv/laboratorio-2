
package ui;

import Exception.LibroException;
import data.Soporte;
import data.Autor;
import data.Miembro;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import servicio.ServicioSoporte;

/**
 *
 * @author fabian.giraldo
 */

public class UI {
    private Scanner sc ;
    private ServicioSoporte servicio;
    
    public UI(){
      this.sc = new Scanner(System.in);
      this.servicio = new ServicioSoporte();
    }
         
    public void imprimirSoportes(ArrayList<Soporte> soportes){
        if(soportes != null){
          for(Soporte soporte: soportes){
              System.out.println(soporte);
          }
        }else{
            System.out.println("No existen soportes actualmente");
        }
    }
    
    public void menu(){
        int opcion = 0; 
        String rutaArchivo = null;
        System.out.println("Bienvenido a la mediateca");
        System.out.println("Opciones. 1. Cargar datos 2. Imprimir. 3. Buscar soportes y pedir pr�stamos.");
        opcion = this.sc.nextInt();
        if(opcion == 1){
           System.out.println("Nombre del archivo: ");
           rutaArchivo = sc.next();
           try {
               this.servicio.cargarSoportes(rutaArchivo);
           } catch (FileNotFoundException ex) {
               System.out.println("El archivo especificado no existe");
           } catch (LibroException ex) {
               System.out.println(ex.getMessage());
           }
        }
        else if(opcion == 2){
           ArrayList<Soporte> soportes = this.servicio.getSoportes();
           this.imprimirSoportes(soportes);
        }
        else if (opcion == 3){
        	System.out.println("�Desea buscar por t�tulo o autor? 1.T�tulo 2.Autor");
        	int eleccion=0;
        	eleccion = sc.nextInt();
        	Soporte soporte = null;
        	if (eleccion==1){
        		System.out.println("Ingrese el t�tulo a buscar:");
                String titulo = sc.next();
                soporte = this.servicio.buscarSoporteTitulo(titulo);
                if(soporte != null){
                    System.out.println(soporte);
                }else{
                    System.out.println("El t�tulo ingresado no es v�lido");
                }
        	}
        	else if(opcion==2){
            	System.out.println("Ingrese el nombre del autor a buscar:");
            	String nombre = sc.next();
            	System.out.println("Ingrese el apellido del autor a buscar:");
            	String apellido = sc.next();
            	Autor autor = new Autor(nombre,apellido);
            	soporte = this.servicio.buscarSoporteAutor(autor);
            	if(soporte != null){
                    System.out.println(soporte);
                }else{
                    System.out.println("El  ingresado no es v�lido");
                }
        	}
        	System.out.println("�Desea pedir prestado el soporte buscado? 1.S� 2.No");
        	int decision=0;
        	int contador=0;
        	int cont=0;
        	int dias=0;
        	int diasAlmacenados[] = new int[3];
        	Soporte[] soportesPrestados = new Soporte[3];
        	if(decision==1){
        		System.out.println("Ingrese su nombre:");
            	String nombre = sc.next();
            	try {
            		Miembro miembro = this.servicio.cargarMiembro(rutaArchivo);
            		if(miembro.getNombre().equals(nombre)){
            			if(contador<4){
            				soportesPrestados[contador]=soporte;
            				contador=+1;
            				System.out.println("�Cu�ntos d�as lo necesita?");
            				while(true){
            					dias = sc.nextInt();
            					if (dias>3){
            						System.out.println("Lo sentimos, el sistema solo permite 3 d�as como tiempo m�ximo de pr�stamo. Escoga un valor adecuado.");
            					}else{
            						diasAlmacenados[cont]=dias;
            						cont=+1;
            						break;
            					}
            				}	
            			}else{
                       		System.out.println("Lo sentimos, el sistema permite unicamente 3 pr�stamos.");
                       		System.out.println("Recuento de pr�stamos:");
                       		System.out.println(soportesPrestados[0] + ", " + diasAlmacenados[0]);
                       		System.out.println(soportesPrestados[1] + ", " + diasAlmacenados[1]);
                       		System.out.println(soportesPrestados[2] + ", " + diasAlmacenados[2]);
                       	}
            		}else{
            			System.out.println("No eres miembro de la mediateca, los sentimos, no puedes hacer pr�stamos");
            		}
                }catch (FileNotFoundException ex) {
                    System.out.println("El archivo especificado no existe");
            	}
            }else{
            	System.out.println("Recuento de pr�stamos:");
           		System.out.println(soportesPrestados[0] + ", " + diasAlmacenados[0]);
           		System.out.println(soportesPrestados[1] + ", " + diasAlmacenados[1]);
           		System.out.println(soportesPrestados[2] + ", " + diasAlmacenados[2]);
            }
        }
        else{
            System.out.println("Opci�n inv�lida");
        }
    }
}
