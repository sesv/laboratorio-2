/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego;

/**
 *
 * @author Estudiante
 */
public abstract class Alienigena {
    private String color;
    private int noOjos;
    private int noVidas;

    public Alienigena(String color, int noOjos, int noVidas) {
        this.color = color;
        this.noOjos = noOjos;
        this.noVidas = noVidas;
    }
    @Override
    public String toString() {
        return "Un alien tiene " + ", color:" + color + ", número de ojos:" + noOjos + " y número de vidas: "+noVidas;
    }
    
}
