/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uml;

/**
 *
 * @author Ma
 */
public class UML {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       Persona p = new Persona("Manuela","Casa","Lunes a Viernes");
       p.asistir();
       Empleado e= new Empleado("Juan", "Lunes y Miercoles", "Hotel", "Edgar Castillo");
       e.asistir();
       e.cobrar("Juan");
       Estudiante s= new Estudiante("Camilo", "apartamento","Lunes, Jueves y Viernes", 8,"B");
       s.asistir();
       s.estudiar("Camilo");
       Administrador a=new Administrador("Luis", "Casa" ,"Lunes a Sábado", "Oxxo", "Gerente");
       a.asistir();
       a.cobrar("Luis");
       a.administrar("Luis");
       Profesor pro=new Profesor("Jairo", "Apartamento" ,"Lunes a Viernes", "Universidad Nacional", "Física");
       pro.asistir();
       pro.cobrar("Jairo");
       pro.enseñar("Jairo");
       
    }
    
}
