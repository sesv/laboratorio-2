/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uml;

/**
 *
 * @author Ma
 */
public class Empleado extends Persona {
    private String jefe;
    
    public Empleado(String nombre,String domicilio, String horario, String jefe) {
        super(nombre, domicilio, horario);
        this.jefe = jefe;
    }
    
    public void cobrar(String nombre){
        System.out.println(nombre+ " cobra su dinero a "+jefe);
    }
}
