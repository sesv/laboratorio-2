/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uml;

/**
 *
 * @author Ma
 */
public class Estudiante extends Persona {
    private int grado;
    private String grupo;

    public Estudiante(String nombre, String domicilio, String horario, int grado, String grupo) {
        super(nombre, domicilio, horario);
        this.grado = grado;
        this.grupo = grupo;
    }
    
    public void estudiar(String nombre){
        System.out.println(nombre+" estudia en el grado "+grado+" y en el grupo "+grupo);
    }
}
