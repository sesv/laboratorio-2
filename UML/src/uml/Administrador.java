/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uml;

/**
 *
 * @author Ma
 */
public class Administrador extends Empleado {
    private String puesto;

    public Administrador(String nombre,String domicilio,String horario, String jefe,String puesto) {
        super(nombre, domicilio, horario, jefe);
        this.puesto = puesto;
    }
    
    public void administrar(String nombre){
        System.out.println(nombre+" administra en su puesto de "+puesto);
    }
}
