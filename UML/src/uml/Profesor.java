/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uml;

/**
 *
 * @author Ma
 */
public class Profesor extends Empleado {
    private String carrera;

    public Profesor(String nombre,String domicilio,String horario, String jefe, String carrera) {
        super(nombre, domicilio, horario,jefe);
        this.carrera = carrera;
    }
    
    public void enseñar(String nombre){
        System.out.println(nombre+" enseña como licenciado en "+carrera);
    }
}
