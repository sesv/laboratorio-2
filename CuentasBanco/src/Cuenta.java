
public class Cuenta {
	protected final String numCuenta;
	protected String nombreCliente;
	protected double saldo;
	
	public Cuenta(String numCuenta, String nombreCliente, double saldo){
		this.numCuenta=numCuenta;
		this.nombreCliente=nombreCliente;
		this.saldo=saldo;
	}
	
	public String getNumCuenta(){
		return this.numCuenta;
	}
	
	public String getNombreCliente(){
		return this.nombreCliente;
	}

	public double getSaldo(){
		return this.saldo;
	}
	
	public void depositar(double deposito){
		this.saldo+=deposito;
		System.out.println("Operación exitosa, su nuevo saldo es "+this.getSaldo());
	}
	
	public void retirar(double retiro){
		if(this.getSaldo()<retiro){
			this.saldo-=retiro;
			System.out.println("Operación exitosa, su saldo restante es "+this.getSaldo());
		}else{
			System.out.println("Saldo insuficiente"+"su saldo es "+this.getSaldo());
		}
	}
	
	public void consultar(){
		System.out.println("\t Numero de cuenta: "+this.getNumCuenta()+"\n \t Nombre del Cliente: "+this.getNombreCliente()
				+"\n \t Saldo: "+this.getSaldo());
	}
}
