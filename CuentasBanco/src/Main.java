import java.util.*;
import java.text.*;

public class Main {

	public static void main(String[] args) {
		HashMap<String, Cuenta> cuentas = new HashMap<>();
		boolean continuar = true;
		while (continuar) {
			java.util.Scanner input = new java.util.Scanner(System.in);
			SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
			System.out.println("Qu� operiaci�n desea realizar? \n \t 1.Crear una cuenta. "
					+ "\n \t 2.Consultar los datos de una cuenta." + "\n \t 3.Retirar/Depositar.");
			int eleccion = input.nextInt();
			switch (eleccion) {
			case 1:
				System.out.println("Seleccione el tipo de cuenta que desea crear:" + "\n \t 1.Ahorros. \t 2.Cheques.");
				eleccion = input.nextInt();
				System.out.println("Ingrese el n�mero de cuenta.");
				String numC = input.next();
				System.out.println("Ingrese el nombre del cliente.");
				String nombC = input.next();
				System.out.println("Ingrese el saldo de la cuenta.");
				double saldo = input.nextDouble();
				switch (eleccion) {
				case 1:
					System.out.println("Ingrese la fecha de vencimiento: \t (dd/MM/yyyy)");
					String fechaV = input.next();
					try {
						Date fechaVencimiento = formatoFecha.parse(fechaV);
						System.out.println(
								"Ingrese el inter�s mensual: (porcentaje, si es 5% ingrese 5. Debe estar entre 0 y 100)");
						double intMes = input.nextInt() / 100;
						if (intMes > 1 || intMes < 0) {
							System.out.println("Valor invalido");
						} else {
							Cuenta c = new CuentaAhorros(numC, nombC, saldo, fechaVencimiento, intMes);
							cuentas.put(numC, c);
						}
					} catch (ParseException e) {
						System.out.println("Eror: Ingrese los datos con el formato indicado.");
					} catch (InputMismatchException e) {
						System.out.println("Error: Esta ingresando valores invalidos.");
					}
					break;
				case 2:
					System.out.println(
							"Ingrese el inter�s por el uso de la chequera.(porcentaje, si es 5% ingrese 5. Debe estar entre 0 y 100)");
					double intC = input.nextDouble() / 100;
					System.out.println(
							"Ingrese el inter�s por expedici�n de cheques con saldo insuficiente.(porcentaje, si es 5% ingrese 5. Debe estar entre 0 y 100)");
					double intCSI = input.nextDouble() / 100;
					if (intC <= 100 && intC >= 0 && intCSI >= 0 && intCSI <= 100) {
						Cuenta c = new CuentaCheques(numC, nombC, saldo, intC, intCSI);
						cuentas.put(numC, c);
					}
					break;
				}
				break;
			case 2:
				System.out.println("Ingrese el n�mero de la cuenta que desea consultar");
				numC = input.next();
				Cuenta c = cuentas.get(numC);
				if (c != null) {
					c.consultar();
				} else {
					System.out.print("No se encontr� ninguna cuenta en el sistema.");
				}

				break;
			case 3:
				System.out.println("Ingrese el numero de la cuenta que desea retirar/depositar");
				numC = input.next();
				c = cuentas.get(numC);
				if (c != null) {
					if (c.getClass().getSimpleName() == "CuentaAhorros") {
						System.out.println("Seleccione la operaci�n que desea realizar: \n \t 1.Deposito normal"
								+ "\n \t 2.Deposito inter�s mensual. \n \t 3.Retirar Ahorros.");
						eleccion = input.nextInt();
						switch (eleccion) {
						case 1:
							System.out.println("Ingrese la cantidad que desea depositar");
							double deposito = input.nextDouble();
							c.depositar(deposito);
							break;
						case 2:
							System.out.println("Ingrese la fecha actual. \t (dd/MM/yyyy)");
							String fechaA = input.next();
							try {
								Date fechaActual = formatoFecha.parse(fechaA);
								Calendar fecha = Calendar.getInstance();
								fecha.setTime(fechaActual);
								if (fecha.get(Calendar.DATE) == 1) {
									c.depositar(((CuentaAhorros) c).getInteresMensual() * c.getSaldo());
								}
							} catch (ParseException e) {
								System.out.println("Eror: Ingrese los datos con el formato indicado.");
							}
							break;
						case 3:
							System.out.println("Ingrese la fecha actual. \t (dd/MM/yyyy");
							fechaA = input.next();
							try {
								Date fechaActual = formatoFecha.parse(fechaA);
								((CuentaAhorros) c).retirarAhorros(fechaActual);
							} catch (ParseException e) {
								System.out.println("Eror: Ingrese los datos con el formato indicado.");
							}
							break;
						default:
							System.out.println("Ingrese una opci�n v�lida");
						}
					} else if (c.getClass().getSimpleName() == "CuentaCheques") {
						System.out.println("Seleccione la operaci�n que desea realizar: \n \t 1.Deposito." +
								"\n \t 2.Retiro.");
						eleccion = input.nextInt();
						switch (eleccion) {
						case 1:
							System.out.println("Ingrese la cantidad que desea depositar.");
							double deposito = input.nextDouble();
							c.depositar(deposito);
							break;
						case 2:
							System.out.println("Ingrese la cantidad que desea retirar.");
							double retiro = input.nextDouble();
							c.retirar(retiro);
							break;
						default:
							System.out.println("Ingrese una opci�n v�lida.");

						}
					} else {
						System.out.println("No se encontr� ninguna cuenta en el sistema.");
					}
				}
				break;
			case 4:
				continuar=false;
				break;
			default:
				System.out.println("Ingrese una opci�n v�lida.");
			}
		}
	}

}
