
public abstract class Pizza {
	public String masa;
	public String salsa;
	
	public Pizza() {
		this.masa= "Regular";
		this.salsa = "Tomate";
	}
	
	public abstract void Hornear();
	public abstract void Cortar();
	
	public void Empacar(){
		System.out.println("La pizza est� empacada en una caja de cart�n");
	}
}
