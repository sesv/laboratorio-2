import java.util.Arrays;

public class Queso extends Pizza{
	public String[] ingredientes;
	
	public Queso(){
		super();
		String[] ingredientes = new String[2];
		ingredientes[0] = "Queso mozarrella fresco";
		ingredientes[1] = "Parmesano";
		this.ingredientes = ingredientes;
	}
	
	public void Hornear(){
		System.out.println("El tiempo de horneado es: 15 minutos");
	}
	
	public void Cortar(){
		System.out.println("La pizza viene cortada en 4 pedazos triangulares");
	}

	@Override
	public String toString() {
		return "Queso [ingredientes=" + Arrays.toString(ingredientes) + ", masa=" + masa + ", salsa=" + salsa + "]";
	}
}
