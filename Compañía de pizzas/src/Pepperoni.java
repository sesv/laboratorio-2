import java.util.Arrays;

public class Pepperoni extends Pizza{
	public String[] ingredientes;

	public Pepperoni() {
		super();
		String[] ingredientes = new String[3];
		ingredientes[0] = "Rodajas de peperoni";
		ingredientes[1] = "Rodajas de cebolla";
		ingredientes[2] = "Queso parmesano rallado";
		this.ingredientes = ingredientes;
	}
	
	public void Hornear(){
		System.out.println("El tiempo de horneado es: 20 minutos");
	}
	
	public void Cortar(){
		System.out.println("La pizza viene cortada en 8 pedazos triangulares");
	}

	@Override
	public String toString() {
		return "Pepperoni [ingredientes=" + Arrays.toString(ingredientes) + ", masa=" + this.masa + ", salsa=" + this.salsa + "]";
	}

}
