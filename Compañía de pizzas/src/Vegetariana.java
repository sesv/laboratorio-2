import java.util.Arrays;

public class Vegetariana extends Pizza{
	public String[] ingredientes;
	
	public Vegetariana(){
		super();
		String[] ingredientes = new String[6];
		ingredientes[0] = "Mozarrella";
		ingredientes[1] = "Parmesano rallado";
		ingredientes[2] = "Cebolla picada";
		ingredientes[3] = "Hongos en rebanadas";
		ingredientes[4] = "Pimienta roja en rodajas";
		ingredientes[5] = "Aceitunas negras rebanadas";
		this.ingredientes = ingredientes;
	}
	
	public void Hornear(){
		System.out.println("El tiempo de horneado es de: 10 minutos");
	}
	
	public void Cortar(){
		System.out.println("La pizza viene cortada en 6 pedazos triangulares");
	}

	@Override
	public String toString() {
		return "Vegetariana [ingredientes=" + Arrays.toString(ingredientes) + ", masa=" + this.masa + ", salsa=" + this.salsa
				+ "]";
	}
	
}
