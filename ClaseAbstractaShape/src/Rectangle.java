
public class Rectangle extends Shape {
	
	public Rectangle(double width, double height){
		super(4);
		this.width=width;
		this.height=height;
		if(height<=0 || width<=0){
			throw new IllegalArgumentException("La base y la altura deben ser ser positivos.");
		}
	}
	@Override
	public double getArea() {
		return this.getWidth()*this.getHeight();
	}

	@Override
	public double getPerimeter() {
		return 2*this.getWidth()+2*this.getHeight();
	}

}
